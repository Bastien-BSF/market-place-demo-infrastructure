# Application Definition 
app_name        = "marketplace" # Do NOT enter any spaces
app_environment = "demo"       # Dev, Test, Staging, Prod, etc

# Network
vpc_cidr           = "10.11.0.0/16"
public_subnet_cidr = "10.11.1.0/24"

# AWS Settings for static auth
aws_access_key = ""
aws_secret_key = ""
aws_region     = ""

# Linux Virtual Machine
linux_instance_type               = "t2.micro"
linux_associate_public_ip_address = true
linux_root_volume_size            = 20
linux_root_volume_type            = "gp2"
